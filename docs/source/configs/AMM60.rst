.. _AMM60:

Atlantic Margin Model 1/60 (AMM60)
==================================

.. figure:: images/amm60.png

Domain
------
The AMM60 configuration is based on the :ref:`AMM7` domain, which spans the region from 340\ :sup:`o`\W-40\ :sup:`o`\N to 13\ :sup:`o`\E- 65\ :sup:`o`\N. The NEMO tripolar grid is rotated such that the equator passes through the domain. This operation results in grid with a resolution of 1.8 km in both pseudo north and east directions. 


References
----------

Warner, J.C., Sherwood, C.R., Arango, H.G., and Signell, R.P.: Performance of four turbulence closure models implemented using a generic length scale method, Ocean Modelling, 8, 81-115, 2005.
